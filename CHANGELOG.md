# Changelog
## [AnonAddy v2.0.3] - 2021-04-19

### Fixed/Improved
- [IMPROVED] Updated libraries
- [IMPROVED] Added "Use Reply-To Header For Replying" to AnonAddy settings
- [IMPROVED] Overall app improvements


## [AnonAddy v2.0.2] - 2021-02-08

### Fixed/Improved
- [IMPROVED] Updated shared domains


## [AnonAddy v2.0.1] - 2021-01-18

### Added
- 📋 [NEW] Sending an email from an alias will now also copy the recipients to the clipboard


## [AnonAddy v2.0.0] - 2021-01-11

### Added
- 🌐 [NEW] Get full control over how often background data such as widgets are refreshed
- 👁️ [NEW] [APP EXCLUSIVE] Watch your aliases to get notified when it forwards a new email
- ✉  [NEW] Quickly send emails from an alias
- 🔤  [NEW] Added random characters alias format
- 💳 [NEW] View the subscription end date
### Fixed/Improved
- 🖌️ [IMPROVED] There are some major and minor UI improvements making the app look even better on phones, tablets and foldables
- 🔐 [IMPROVED] The app just got even more secure as it now also requires authentication for actions being executed from the widget or intents
- 🧈 [IMPROVED] Lot's of love and butter has gone into this release to make the app smoother and more lightweight than ever before. Especially the widget got a big improvement
- 🦋 [IMPROVED] Minor bugs have been fixed


## [AnonAddy v1.1.3] - 2020-12-08

### Fixed/Improved
- 🐛 [BUG FIX] Deleted alias section is not collapsed by default
- 🙋 [FEATURE REQUEST] Switch between panels by swiping left/right


## [AnonAddy v1.1.2] - 2020-11-21

### Fixed/Improved
- 🖥️ Optimized UI for big screens
- 🐛 [BUG FIX] Improved responsiveness when switching fragments
- 🙋 [FEATURE REQUEST] Added link to gitlab page
- 🙋 [FEATURE REQUEST] Separated deleted aliases into a different section


## [AnonAddy v1.1.1] - 2020-10-24

### Fixed/Improved
-  Added the forgotten piece of the custom alias format >.<
- Fixed crash at double Biometrics authentication prompt when dark mode was enabled


## [AnonAddy v1.1.0] - 2020-10-24

### Added
- 📋 Added new rule-editor (beta)
 -  The feature is still in beta, and might not be available on the hosted instance -  For self-hosted instanced, enable the rule feature in order to use this editor
 - 🔤 Added catch-all switch for AnonAddy v0.4.0>
 - ⌨ Added custom alias format option
 - 💸 Added subscription check for random words alias format
 - 💌 Show changelog on update
 - 🛠️ Added version check (for self hosted instances)


### Fixed/Improved
- 🔎 Improved the search function
- 🌟 UI improvements