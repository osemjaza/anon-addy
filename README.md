
<h1 align="center">AnonAddy for Android</h1>

<p align="center">
Easily create and manage your AnonAddy aliases, recipients and more from your phone or tablet with this <b>sexy</b> AnonAddy app for Android.
 </br><p align="center">
<img src="static/banner.png"/>
</p>

<p align="center">
<a href='https://play.google.com/store/apps/details?id=host.stjin.anonaddy&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'  height="80"/></a>
<a href="https://f-droid.org/packages/host.stjin.anonaddy"> <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">
</a>
</p>

## Requirements

> AnonAddy

- AnonAddy v0.6.2 or higher (current version of AnonAddy for Android has been tested with v0.6.2)

> Android

- Android 6.0 or higher

## Characteristics

> Exciting ✨

- Connect to the [AnonAddy](https://anonaddy.com/) instance or your own hosted instance
- A unique, sexy and sleek design, based on the [Material Design](https://material.io/) language
- Support for Deep Link to easily deactivate aliases from the email banner
- Lock the app using Biometrics
- App exclusive features like watching aliases for new emails
- Enable error logging to *locally* store exceptions for easy troubleshooting
- Widgets!

> Seriously 👓

- **Security**.
 - Encrypted preferences, your API key and other AnonAddy related settings are securely stored on your device using the [AndroidX crypto library](https://developer.android.com/jetpack/androidx/releases/security) - No stats, buried points or Device IDs, or even crash reporting (so if you get a crash, please share the crash info with me >_<). - I am forgoing the convenient third-party collection SDK and various stats just so you can use it with confidence.  **What's yours is yours**.

 > Manage (add, edit, delete)

- Aliases
- Recipients
- Domains
- Usernames
- Rules

## Download

Developer-led download channels:

>Recommended (access to beta's, automatic updates and support me :) )
- [Google Play](https://play.google.com/store/apps/details?id=host.stjin.anonaddy)
 - ⚠️Note: this version charges but the code is identical, so if you'd like to buy the author a cup of tea (or if you're more headstrong) then go here and download it, otherwise choose another source :)

>Other sources
- [Gitlab release](https://gitlab.com/Stjin/anonaddy-android/-/releases)
- [F-Droid](https://f-droid.org/packages/host.stjin.anonaddy)

## UI
- Designed by my lovely UI guru @JustPlayingHard (💙)
 - Anything you think doesn't work well is my bad as I recreated the UI drafts into XML.

## Open Source License.
Third party libraries used.
- [androidx.biometric](https://developer.android.com/jetpack/androidx/releases/biometric)
- [androidx.security.crypto](https://developer.android.com/jetpack/androidx/releases/security)
- [androidx.work](https://developer.android.com/jetpack/androidx/releases/work)
- [airbnb/lottie-android](https://github.com/airbnb/lottie-android)
- [kittinunf/fuel](https://github.com/kittinunf/fuel)
- [sharish/ShimmerRecyclerView](https://github.com/sharish/ShimmerRecyclerView)
- [yuriy-budiyev/code-scanner](https://github.com/yuriy-budiyev/code-scanner)
- [futuredapp/donut](https://github.com/futuredapp/donut)


## Feedback and contribution
I welcome your comments and suggestions in the issues section, or you can contribute your code by submitting a PR directly to me.
Of course, you can also contact the developer directly via telegram or email and I'll get back to you shortly.

## Donation
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=26D39SEWQLBHW)

## Privacy policy
[Privacy policy](https://gitlab.com/Stjin/anonaddy-android/-/blob/master/PrivacyPolicy.md)