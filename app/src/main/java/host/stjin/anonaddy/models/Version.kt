package host.stjin.anonaddy.models

data class Version(
    val major: Int?,
    val minor: Int?,
    val patch: Int?,
    val version: String?
)