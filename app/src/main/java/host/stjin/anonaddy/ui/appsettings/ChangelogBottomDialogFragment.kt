package host.stjin.anonaddy.ui.appsettings

import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import host.stjin.anonaddy.R
import host.stjin.anonaddy.databinding.BottomsheetChangelogBinding


class ChangelogBottomDialogFragment : BottomSheetDialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return dialog
    }

    private var _binding: BottomsheetChangelogBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomsheetChangelogBinding.inflate(inflater, container, false)
        val root = binding.root


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            binding.bsChangelogTextview.text = Html.fromHtml(
                context?.resources?.getString(R.string.app_changelog),
                Html.FROM_HTML_MODE_LEGACY
            )
        } else {
            binding.bsChangelogTextview.text =
                Html.fromHtml(context?.resources?.getString(R.string.app_changelog))
        }
        return root

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance(): ChangelogBottomDialogFragment {
            return ChangelogBottomDialogFragment()
        }
    }
}