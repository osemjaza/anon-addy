package host.stjin.anonaddy.ui.usernames

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import host.stjin.anonaddy.BaseActivity
import host.stjin.anonaddy.NetworkHelper
import host.stjin.anonaddy.R
import host.stjin.anonaddy.SettingsManager
import host.stjin.anonaddy.adapter.UsernameAdapter
import host.stjin.anonaddy.databinding.ActivityUsernameSettingsBinding
import host.stjin.anonaddy.databinding.AnonaddyCustomDialogBinding
import host.stjin.anonaddy.models.User
import host.stjin.anonaddy.models.UserResource
import host.stjin.anonaddy.ui.appsettings.logs.LogViewerActivity
import host.stjin.anonaddy.ui.usernames.manage.ManageUsernamesActivity
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UsernamesSettingsActivity : BaseActivity(), AddUsernameBottomDialogFragment.AddUsernameBottomDialogListener {

    private var networkHelper: NetworkHelper? = null
    private var settingsManager: SettingsManager? = null
    private var shouldAnimateRecyclerview: Boolean = true

    private val addUsernameFragment: AddUsernameBottomDialogFragment = AddUsernameBottomDialogFragment.newInstance()

    private lateinit var binding: ActivityUsernameSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUsernameSettingsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setupToolbar(binding.activityUsernameSettingsToolbar)

        settingsManager = SettingsManager(true, this)
        networkHelper = NetworkHelper(this)

        // Set stats right away, update later
        setStats()

        setOnClickListener()
        // Called on OnResume()
        // getDataFromWeb()
    }

    private fun setOnClickListener() {
        binding.activityUsernameSettingsAddUsername.setOnClickListener {
            if (!addUsernameFragment.isAdded) {
                addUsernameFragment.show(
                    supportFragmentManager,
                    "addUsernameFragment"
                )
            }
        }
    }

    private fun getDataFromWeb() {
        binding.activityUsernameSettingsLL1.visibility = View.VISIBLE
        binding.activityUsernameSettingsRLLottieview.visibility = View.GONE

        // Get the latest data in the background, and update the values when loaded
        GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
            getAllUsernames()
            getUserResource()
        }
    }

    private suspend fun getUserResource() {
        networkHelper?.getUserResource { user: UserResource?, result: String? ->
            if (user != null) {
                User.userResource = user
                setStats()
            } else {
                val snackbar =
                    Snackbar.make(
                        binding.activityUsernameSettingsLL,
                        resources.getString(R.string.error_obtaining_user) + "\n" + result,
                        Snackbar.LENGTH_SHORT
                    )

                if (SettingsManager(false, this).getSettingsBool(SettingsManager.PREFS.STORE_LOGS)) {
                    snackbar.setAction(R.string.logs) {
                        val intent = Intent(this, LogViewerActivity::class.java)
                        startActivity(intent)
                    }
                }
                snackbar.show()
            }
        }
    }

    private fun setStats() {
        binding.activityUsernameSettingsRLCountText.text =
            resources.getString(R.string.you_ve_used_d_out_of_d_usernames, User.userResource.username_count, User.userResource.username_limit)
        binding.activityUsernameSettingsAddUsername.isEnabled = User.userResource.username_count < User.userResource.username_limit
    }

    private suspend fun getAllUsernames() {
        binding.activityUsernameSettingsAllUsernamesRecyclerview.apply {

            layoutManager = if (context.resources.getBoolean(R.bool.isTablet)){
                // set a GridLayoutManager for tablets
                GridLayoutManager(this@UsernamesSettingsActivity, 2)
            } else {
                LinearLayoutManager(this@UsernamesSettingsActivity)
            }

            if (shouldAnimateRecyclerview) {
                shouldAnimateRecyclerview = false
                val resId: Int = R.anim.layout_animation_fall_down
                val animation = AnimationUtils.loadLayoutAnimation(context, resId)
                binding.activityUsernameSettingsAllUsernamesRecyclerview.layoutAnimation = animation
            }


            networkHelper?.getAllUsernames { list ->
                // Sorted by created_at automatically
                //list?.sortByDescending { it.emails_forwarded }

                if (list != null) {

                    if (list.size > 0) {
                        binding.activityUsernameSettingsNoUsernames.visibility = View.GONE
                    } else {
                        binding.activityUsernameSettingsNoUsernames.visibility = View.VISIBLE
                    }

                    val usernamesAdapter = UsernameAdapter(list)
                    usernamesAdapter.setClickListener(object : UsernameAdapter.ClickListener {

                        override fun onClickSettings(pos: Int, aView: View) {
                            val intent = Intent(context, ManageUsernamesActivity::class.java)
                            intent.putExtra("username_id", list[pos].id)
                            startActivity(intent)
                        }


                        override fun onClickDelete(pos: Int, aView: View) {
                            deleteUsername(list[pos].id, context)
                        }

                    })
                    adapter = usernamesAdapter
                    binding.activityUsernameSettingsAllUsernamesRecyclerview.hideShimmerAdapter()
                } else {
                    binding.activityUsernameSettingsLL1.visibility = View.GONE
                    binding.activityUsernameSettingsRLLottieview.visibility = View.VISIBLE
                }
            }

        }

    }


    lateinit var dialog: AlertDialog
    private fun deleteUsername(id: String, context: Context) {
        val anonaddyCustomDialogBinding = AnonaddyCustomDialogBinding.inflate(LayoutInflater.from(this), null, false)

        // create an alert builder
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setView(anonaddyCustomDialogBinding.root)
        dialog = builder.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        anonaddyCustomDialogBinding.dialogTitle.text = context.resources.getString(R.string.delete_username)
        anonaddyCustomDialogBinding.dialogText.text = context.resources.getString(R.string.delete_username_desc_confirm)
        anonaddyCustomDialogBinding.dialogPositiveButton.text =
            context.resources.getString(R.string.delete_username)
        anonaddyCustomDialogBinding.dialogPositiveButton.setOnClickListener {
            anonaddyCustomDialogBinding.dialogProgressbar.visibility = View.VISIBLE
            anonaddyCustomDialogBinding.dialogError.visibility = View.GONE
            anonaddyCustomDialogBinding.dialogNegativeButton.isEnabled = false
            anonaddyCustomDialogBinding.dialogPositiveButton.isEnabled = false

            GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
                deleteUsernameHttpRequest(id, context, anonaddyCustomDialogBinding)
            }
        }
        anonaddyCustomDialogBinding.dialogNegativeButton.setOnClickListener {
            dialog.dismiss()
        }
        // create and show the alert dialog
        dialog.show()
    }

    private suspend fun deleteUsernameHttpRequest(id: String, context: Context, anonaddyCustomDialogBinding: AnonaddyCustomDialogBinding) {
        networkHelper?.deleteUsername({ result ->
            if (result == "204") {
                dialog.dismiss()
                getDataFromWeb()
            } else {
                anonaddyCustomDialogBinding.dialogProgressbar.visibility = View.INVISIBLE
                anonaddyCustomDialogBinding.dialogError.visibility = View.VISIBLE
                anonaddyCustomDialogBinding.dialogNegativeButton.isEnabled = true
                anonaddyCustomDialogBinding.dialogPositiveButton.isEnabled = true
                anonaddyCustomDialogBinding.dialogError.text =
                    context.resources.getString(R.string.s_s, context.resources.getString(R.string.error_deleting_username), result)
            }
        }, id)
    }

    override fun onAdded() {
        addUsernameFragment.dismiss()
        // Get the latest data in the background, and update the values when loaded
        getDataFromWeb()
    }

    override fun onResume() {
        super.onResume()
        // Get the latest data in the background, and update the values when loaded
        getDataFromWeb()
    }
}